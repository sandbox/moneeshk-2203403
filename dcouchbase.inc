<?php

/*
 * Core dcouchbase functions required by:
 *   couchbase.inc
 *   couchbase.db.inc
 *   session-couchbase.inc
 *   session-couchbase.db.inc
 */

global $_couchbase_statistics;
$_couchbase_statistics = array();

/*
 * A couchbase API for Drupal.
 */

/**
 *  Place an item into couchbase
 *
 * @param $key The string with which you will retrieve this item later.
 * @param $value The item to be stored.
 * @param $exp Parameter expire is expiration time in seconds. If it's 0, the
 *   item never expires (but couchbased server doesn't guarantee this item to be
 *   stored all the time, it could be deleted from the cache to make place for
 *   other items).
 * @param $bin The name of the Drupal subsystem that is making this call.
 *   Examples could be 'cache', 'alias', 'taxonomy term' etc. It is possible to
 *   map different $bin values to different couchbase servers.
 * @param $ch Optionally pass in the couchbase object.  Normally this value is
 *   determined automatically based on the bin the object is being stored to.
 *
 * @return bool
 */
function dcouchbase_set($key, $value, $exp = 0, $bin = 'cache', $ch = NULL) {
  global $_couchbase_statistics;
  $full_key = dcouchbase_key($key, $bin);
  $_couchbase_statistics[] = array('set', $bin, $full_key, '');
  if ($ch || ($ch = dcouchbase_object($bin))) {
    if ($ch instanceof Couchbase) {
      return $ch->set($full_key, $value, $exp);
    }
    else {
      return $ch->set($full_key, $value, $exp);
    }
  }
  return FALSE;
}

/**
 *  Add an item into couchbase
 *
 * @param $key The string with which you will retrieve this item later.
 * @param $value The item to be stored.
 * @param $exp Parameter expire is expiration time in seconds. If it's 0, the
 *   item never expires (but couchbased server doesn't guarantee this item to be
 *   stored all the time, it could be deleted from the cache to make place for
 *   other items).
 * @param $bin The name of the Drupal subsystem that is making this call.
 *   Examples could be 'cache', 'alias', 'taxonomy term' etc. It is possible
 *   to map different $bin values to different couchbase servers.
 * @param $ch Optionally pass in the couchbase object.  Normally this value is
 *   determined automatically based on the bin the object is being stored to.
 * @param $flag If using the older couchbase PECL extension as opposed to the
 *   newer couchbased PECL extension, the MEMCACHE_COMPRESSED flag can be set
 *   to use zlib to store a compressed copy of the item.  This flag option is
 *   completely ignored when using the newer couchbased PECL extension.
 *
 * @return bool
 */
function dcouchbase_add($key, $value, $exp = 0, $bin = 'cache', $ch = NULL, $flag = FALSE) {
  global $_couchbase_statistics;
  $full_key = dcouchbase_key($key, $bin);
  $_couchbase_statistics[] = array('add', $bin, $full_key, '');
  if ($ch || ($ch = dcouchbase_object($bin))) {
    if ($ch instanceof Couchbase) {
      return $ch->add($full_key, $value, $exp);
    }
    else {
      return $ch->add($full_key, $value, $exp);
    }
  }
  return FALSE;
}

/**
 * Retrieve a value from the cache.
 *
 * @param $key The key with which the item was stored.
 * @param $bin The bin in which the item was stored.
 *
 * @return The item which was originally saved or FALSE
 */
function dcouchbase_get($key, $bin = 'cache', $ch = NULL) {
  global $_couchbase_statistics;
  $result = FALSE;
  $full_key = dcouchbase_key($key, $bin);
  $statistics = array('get', $bin, $full_key);
  $success = '0';
  if ($ch || $ch = dcouchbase_object($bin)) {
    $track_errors = ini_set('track_errors', '1');
    $php_errormsg = '';

    $result = @$ch->get($full_key);
    $_couchbase_statistics[] = $statistics;

    if (!empty($php_errormsg)) {
      register_shutdown_function('watchdog', 'couchbase', 'Exception caught in dcouchbase_get: !msg', array('!msg' => $php_errormsg), WATCHDOG_WARNING);
      $php_errormsg = '';
    }
    ini_set('track_errors', $track_errors);
  }

  return $result;
}

/**
 * Retrieve multiple values from the cache.
 *
 * @param $keys The keys with which the items were stored.
 * @param $bin The bin in which the item was stored.
 *
 * @return The item which was originally saved or FALSE
 */
function dcouchbase_get_multi($keys, $bin = 'cache', $ch = NULL) {
  global $_couchbase_statistics;
  $full_keys = array();
  $statistics = array();
  foreach ($keys as $key => $cid) {
    $full_key = dcouchbase_key($cid, $bin);
    $statistics[$full_key] = array('getMulti', $bin, $full_key);
    $full_keys[$cid] = $full_key;
  }
  $results = array();
  if ($ch || ($ch = dcouchbase_object($bin))) {
    if ($ch instanceof Couchbase) {
      $results = $ch->getMulti($full_keys);
    }
    elseif ($ch instanceof Couchbase) {
      $track_errors = ini_set('track_errors', '1');
      $php_errormsg = '';

      $results = @$ch->get($full_keys);
      if (!empty($php_errormsg)) {
        register_shutdown_function('watchdog', 'couchbase', 'Exception caught in dcouchbase_get_multi: !msg', array('!msg' => $php_errormsg), WATCHDOG_WARNING);
        $php_errormsg = '';
      }
      ini_set('track_errors', $track_errors);
    }
  }
  foreach ($statistics as $key => $values) {
    $values[] = isset($results[$key]) ? '1': '0';
    $_couchbase_statistics[] = $values;
  }

  // If $results is FALSE, convert it to an empty array.
  if (!$results) {
    $results = array();
  }

  // Convert the full keys back to the cid.
  $cid_results = array();
  $cid_lookup = array_flip($full_keys);
  foreach ($results as $key => $value) {
    $cid_results[$cid_lookup[$key]] = $value;
  }
  return $cid_results;
}

/**
 * Deletes an item from the cache.
 *
 * @param $key The key with which the item was stored.
 * @param $bin The bin in which the item was stored.
 *
 * @return Returns TRUE on success or FALSE on failure.
 */
function dcouchbase_delete($key, $bin = 'cache', $ch = NULL) {
  global $_couchbase_statistics;
  $full_key = dcouchbase_key($key, $bin);
  $_couchbase_statistics[] = array('delete', $bin, $full_key, '');
  if ($ch || ($ch = dcouchbase_object($bin))) {
    return $ch->delete($full_key, 0);
  }
  return FALSE;
}

/**
 * Immediately invalidates all existing items. dcouchbase_flush doesn't actually free any
 * resources, it only marks all the items as expired, so occupied memory will be overwritten by
 * new items.
 *
 * @param $bin The bin to flush. Note that this will flush all bins mapped to the same server
 *   as $bin. There is no way at this time to empty just one bin.
 *
 * @return Returns TRUE on success or FALSE on failure.
 */
function dcouchbase_flush($bin = 'cache', $ch = NULL) {
  global $_couchbase_statistics;
  $_couchbase_statistics[] = array('flush', $bin, '', '');
  if ($ch || ($ch = dcouchbase_object($bin))) {
    return couchbase_flush($ch);
  }
}

function dcouchbase_stats($stats_bin = 'cache', $stats_type = 'default', $aggregate = FALSE) {
  $couchbase_bins = variable_get('couchbase_bins', array('cache' => 'default'));
  // The stats_type can be over-loaded with an integer slab id, if doing a
  // cachedump.  We know we're doing a cachedump if $slab is non-zero.
  $slab = (int)$stats_type;

  foreach ($couchbase_bins as $bin => $target) {
    if ($stats_bin == $bin) {
      if ($ch = dcouchbase_object($bin)) {
        if ($ch instanceof Couchbase) {
          $stats[$bin] = $ch->getStats();
        }
        // The PHP Couchbase extension 3.x version throws an error if the stats
        // type is NULL or not in {reset, malloc, slabs, cachedump, items,
        // sizes}. If $stats_type is 'default', then no parameter should be
        // passed to the Couchbase couchbase_get_extended_stats() function.
        else if ($ch instanceof Couchbase) {
          if ($stats_type == 'default' || $stats_type == '') {
            $stats[$bin] = $ch->getExtendedStats();
          }
          // If $slab isn't zero, then we are dumping the contents of a
          // specific cache slab.
          else if (!empty($slab))  {
            $stats[$bin] = $ch->getStats('cachedump', $slab);
          }
          else {
            $stats[$bin] = $ch->getExtendedStats($stats_type);
          }
        }
      }
    }
  }
  // Optionally calculate a sum-total for all servers in the current bin.
  if ($aggregate) {
    // Some variables don't logically aggregate.
    $no_aggregate = array('pid', 'time', 'version', 'pointer_size', 'accepting_conns', 'listen_disabled_num');
    foreach($stats as $bin => $servers) {
      if (is_array($servers)) {
        foreach ($servers as $server) {
          if (is_array($server)) {
            foreach ($server as $key => $value) {
              if (!in_array($key, $no_aggregate)) {
                if (isset($stats[$bin]['total'][$key])) {
                  $stats[$bin]['total'][$key] += $value;
                }
                else {
                  $stats[$bin]['total'][$key] = $value;
                }
              }
            }
          }
        }
      }
    }
  }
  return $stats;
}

/**
 * Returns an Couchbase object based on the bin requested. Note that there is
 * nothing preventing developers from calling this function directly to get the
 * Couchbase object. Do this if you need functionality not provided by this API
 * or if you need to use legacy code. Otherwise, use the dcouchbase (get, set,
 * delete, flush) API functions provided here.
 *
 * @param $bin The bin which is to be used.
 *
 * @param $flush Rebuild the bin/server/cache mapping.
 *
 * @return an Couchbase object or FALSE.
 */
function dcouchbase_object($bin = NULL, $flush = FALSE) {
  static $extension, $couchbaseCache = array(), $couchbase_servers, $couchbase_bins, $couchbase_persistent, $failed_connection_cache;

  if (!isset($extension)) {
    // If an extension is specified in settings.php, use that when available.
    $preferred = variable_get('couchbase_extension', NULL);
    if (isset($preferred) && class_exists($preferred)) {
      $extension = $preferred;
    }
    // If no extension is set, default to Couchbase.
    // The Couchbase extension has some features that the older extension lacks
    // but also an unfixed bug that affects cache clears.
    // @see http://pecl.php.net/bugs/bug.php?id=16829
    elseif (class_exists('Couchbase')) {
      $extension = 'Couchbase';
    }

    // Indicate whether to connect to couchbase using a persistent connection.
    // Note: this only affects the Couchbase PECL extension, and does not
    // affect the Couchbase PECL extension.  For a detailed explanation see:
    //  http://drupal.org/node/822316#comment-4427676
    if (!isset($couchbase_persistent)) {
      $couchbase_persistent = variable_get('couchbase_persistent', FALSE);
    }
  }

  if (empty($couchbaseCache) || empty($couchbaseCache[$bin])) {
    // $couchbase_servers and $couchbase_bins originate from settings.php.
    // $couchbase_servers_custom and $couchbase_bins_custom get set by
    // couchbase.module. They are then merged into $couchbase_servers and
    // $couchbase_bins, which are statically cached for performance.
    if (empty($couchbase_servers)) {
      // Values from settings.php
      $couchbase_servers = variable_get('couchbase_servers', array('127.0.0.1:8091' => 'default'));
      $couchbase_bins    = variable_get('couchbase_bins', array('cache' => 'default'));
    }
    // A variable to track whether we've connected to the first server.
    $init = FALSE;
    // If there is no cluster for this bin in $couchbase_bins, cluster is 'default'.
    $cluster = empty($couchbase_bins[$bin]) ? 'default' : $couchbase_bins[$bin];

    // If this bin isn't in our $couchbase_bins configuration array, and the
    // 'default' cluster is already initialized, map the bin to 'cache' because
    // we always map the 'cache' bin to the 'default' cluster.
    if (empty($couchbase_bins[$bin]) && !empty($couchbaseCache['cache'])) {
      $couchbaseCache[$bin] = &$couchbaseCache['cache'];
    }
    else {
      // Create a new Couchbase object. Each cluster gets its own Couchbase object.
      if ($extension == 'Couchbase') {
        $couchbase = new Couchbase('127.0.0.1:8091','','','default');
        $init = TRUE;
      }
      else {
        drupal_set_message(t('You must enable the PECL couchbased or couchbase extension to use couchbase.inc.'), 'error');
        return;
      }
      
      // Link all the servers to this cluster.
      if (!$init) {
            // Ensure we use an available t() function.
            $t = get_t();
            $error_msg = $t(
              'Failed to connect to couchbase server: %server',
              array('%server' => $s)
            );
            // We can't use watchdog because this happens in a bootstrap phase
            // where watchdog is non-functional. Thus use trigger_error() to
            // start drupal_error_handler().
            trigger_error($error_msg, E_USER_ERROR);
            $failed_connection_cache[$s] = FALSE;
          }

      if ($init) {
        // Map the current bin with the new Couchbase object.
        $couchbaseCache[$bin] = $couchbase;

        // Now that all the servers have been mapped to this cluster, look for
        // other bins that belong to the cluster and map them too.
        foreach ($couchbase_bins as $b => $c) {
          if ($c == $cluster && $b != $bin) {
            // Map this bin and cluster by reference.
            $couchbaseCache[$b] = &$couchbaseCache[$bin];
          }
        }
      }
    }
  }

  return empty($couchbaseCache[$bin]) ? FALSE : $couchbaseCache[$bin];
}

function dcouchbase_key($key, $bin = 'cache') {
  $prefix = '';
  if ($prefix = variable_get('couchbase_key_prefix', '')) {
    $prefix .= '-';
  }
  // When simpletest is running, emulate the simpletest database prefix here
  // to avoid the child site setting cache entries in the parent site.
  if (isset($GLOBALS['drupal_test_info']['test_run_id'])) {
    $prefix .= $GLOBALS['drupal_test_info']['test_run_id'];
  }
  $full_key = urlencode($prefix . $bin . '-' . $key);

  // Couchbase only supports key lengths up to 250 bytes.  If we have generated
  // a longer key, hash it with sha1 which will shrink the key down to 40 bytes
  // while still keeping it unique.
  if (strlen($full_key) > 250) {
    $full_key = $prefix . $bin . '-' . sha1($key);
  }

  return $full_key;
}
